// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html
declare var firebase:any;
import data from './gamedata'
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {


    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        
    }
    private login(){
        let emailbox=this.node.getChildByName("loginbox").getChildByName("email");
        let passwordbox=this.node.getChildByName("loginbox").getChildByName("password");
        let email=emailbox.getComponent(cc.EditBox).string;
        let password=passwordbox.getComponent(cc.EditBox).string;
        firebase.auth().signInWithEmailAndPassword(email, password).then(function(user_token){
            let user=user_token.user;
            let get_name;
            let user_ref=firebase.database().ref('user/'+user.uid);
            user_ref.once('value').then(function(child){
                let val=child.val();
                get_name=val.name;
                let ref=firebase.database().ref('data/'+get_name);
                ref.once('value').then(function(snapshot){
                    let val=snapshot.val();
                    let userdata=cc.find('gamedata').getComponent(data);
                    userdata.name=snapshot.key;
                    cc.director.loadScene("level_select");
            });
            });
        }).catch(function(error){
            email='';
            password='';
            let errorMessage = error.message;
            alert("login failed:"+errorMessage);
        });
    }
    private signup(){
        let namebox=this.node.getChildByName("loginbox").getChildByName("name");
        let emailbox=this.node.getChildByName("loginbox").getChildByName("email");
        let passwordbox=this.node.getChildByName("loginbox").getChildByName("password");
        let name=namebox.getComponent(cc.EditBox).string;
        let email=emailbox.getComponent(cc.EditBox).string;
        let password=passwordbox.getComponent(cc.EditBox).string;
        let check;
        firebase.database().ref('data/'+name).once('value').then(function(snapshot){
            check=snapshot.val();
            if(check!=null){
                alert('The username has been used.');
            }
            else{
                firebase.auth().createUserWithEmailAndPassword(email,password).then(function(user_token){
                    let user=user_token.user;
                    let ref=firebase.database().ref('data/'+name);
                    let user_ref=firebase.database().ref('user/'+user.uid);
                    ref.set({
                        score1:0
                    });
                    user_ref.set({
                        name:name
                    });
                    let userdata=cc.find('gamedata').getComponent(data);
                    userdata.name=name;
                    cc.director.loadScene("level_select");
                }).catch(function(error){
                    email='';
                    password='';
                    name='';
                    let errorMessage = error.message;
                    alert("login failed:"+errorMessage);
                });
            }
        });
    }
    // update (dt) {}
}
