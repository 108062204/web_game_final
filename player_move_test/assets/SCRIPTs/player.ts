// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class player extends cc.Component {

    private anim = null; //this will use to get animation component
    private rigid = null; //this will use to get rigidbody component
    private jump_speed = 300;
    private walk_speed = 100;
    private animateState = null; //this will use to record animationState
    private on_ground = true;

    private playerSpeed: number = 0;

    private aDown: boolean = false; // key for player to go left

    private dDown: boolean = false; // key for player to go right

    //private dDown: boolean = false; // key for player to shoot

    private _Down: boolean = false; // key for player to jump

    // LIFE-CYCLE CALLBACKS:
    private spawn_x : number = -444;
    private spawn_y : number = -180;
    private is_dead : boolean = false;
    private is_reborn : boolean = false;
    private rebornTime : number =1;
    // onLoad () {}

    onLoad () {
        this.anim = this.getComponent(cc.Animation);
        this.rigid = this.getComponent(cc.RigidBody);
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getPhysicsManager().debugDrawFlags = 1;
        //p.gravity = cc.v2(0,0);

    };
    start() 
    {
        // add key down and key up event
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }
    update(dt)
    {
        this.node.x += this.playerSpeed * dt;  //move player

        this.playerAnimation();
        this.update_play_move();
    }

    onKeyDown(event) 
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.a:

                this.aDown = true;

                this.dDown = false;

                break;

            case cc.macro.KEY.d:

                this.dDown = true;

                this.aDown = false;

                break;

            case cc.macro.KEY.space:

                this._Down = true;

                break;
        }
    }

    onKeyUp(event)
    {
        switch(event.keyCode) 
        {
            case cc.macro.KEY.a:
                console.log("released a");
                this.aDown = false;

                break;

            case cc.macro.KEY.d:
                console.log("released d");

                this.dDown = false;

                break;

            case cc.macro.KEY.space:
                console.log("released space");

                this._Down = false;

                break;
        }
    }
    private playerAnimation()
    {
        if(this.anim.getAnimationState('big_player_dead').isPlaying)

            console.log("oy");
        if(this.is_dead  &&!this.is_reborn){
            if(this.animateState == null || this.animateState.name != 'big_player_dead') // when first call or last animation is shoot or idle
            {
                    console.log("play reborn");

                    this.animateState = this.anim.play('big_player_dead');
                    return;
            }
        }
        else if(!this.anim.getAnimationState('big_player_dead').isPlaying||!this.anim.getAnimationState('big_player_dead').isPlaying) // player can move only when shoot animation finished
        {
            if(this.aDown)
            {
                //this.node.scaleX = -1;

                //this.playerSpeed = -1*this.walk_speed;

                if(this.animateState == null || this.animateState.name != 'big_player_walk') // when first call or last animation is shoot or idle
                    this.animateState = this.anim.play('big_player_walk');
            }
            if(this.dDown)
            { 
                //this.node.scaleX = 1;

                //this.playerSpeed = this.walk_speed;

                if(this.animateState == null || this.animateState.name != 'big_player_walk') // when first call or last animation is shoot or idle
                    this.animateState = this.anim.play('big_player_walk');
                
            }
            if(this._Down){//jump
                //let last_v = this.rigid.linearVelocity;

                //this.rigid.linearVelocity =cc.v2(last_v.x,this.jump_speed) ;
                if(this.animateState == null || this.animateState.name != 'big_player_jump') // when first call or last animation is shoot or idle    
                    if(this.on_ground == true)
                        this.animateState = this.anim.play('big_player_jump');

            }

            if((!this.aDown && !this.dDown && !this._Down)||(
                this.aDown && this.dDown))
            {
                //if no key is pressed, stop all animations exclude shoot animation
                if(this.animateState != null)
                {
                    this.anim.play('big_player_idle');
                    this.animateState = null;
                    cc.log('Animation finished.');
                }
                //this.playerSpeed = 0;
            }
            
            
        }
    }
    private update_play_move(){
        
        if(this.is_dead  &&!this.is_reborn){
            this.node.getComponent(cc.RigidBody).linearVelocity = cc.v2(0, 100);
            this.is_reborn = true;
            this.scheduleOnce(function(){
                this.is_dead = false;
                this.is_reborn = false;
                this.node.position = cc.v2(this.spawn_x, this.spawn_y);
                console.log("player reborned")
            }, this.rebornTime);
            return;
        }
        if(this.is_dead){
            this.playerSpeed = 0;
            return;
        } 
        if(this.aDown){
            this.node.scaleX = -1;
            this.playerSpeed = -1*this.walk_speed;
        }
        if(this.dDown){ 
            this.node.scaleX = 1;
            this.playerSpeed = this.walk_speed;
        }
        if(this._Down){//jump
            if(this.on_ground == true){
                let last_v = this.rigid.linearVelocity;
                this.rigid.linearVelocity =cc.v2(last_v.x,this.jump_speed) ; 
                this.on_ground = false;
            }
           
        }

        if((!this.aDown && !this.dDown && !this._Down)||(
            this.aDown && this.dDown                    ))
        {
            //if no key is pressed, stop all animations exclude shoot animation
            if(this.animateState != null)
            {
                this.anim.play('big_player_idle');
                this.animateState = null;
                cc.log('Animation finished.');
            }
            this.playerSpeed = 0;
        }
            
    }

    onBeginContact(contact, self, other) {
        console.log(other.node.name);
        if(other.node.name == "ground") {
            if(this.on_ground == false){
                cc.log("player hits the ground");
            }    
            this.on_ground = true;
        } 
        else if(other.node.name == "monster"){
            contact.getWorldManifold().normal;
            let check = contact.getWorldManifold().normal;
            if(check.y<0){//step
                console.log("step on ")
                //other.killed();
                let last_v = this.rigid.linearVelocity;
                this.rigid.linearVelocity =cc.v2(last_v.x,this.jump_speed) ; 
                this.on_ground = false;
            }
            else {
                this.damage();
            }
        }
        else if(other.node.name == "end"){
            cc.director.loadScene("game_won");

        }
    }
    private damage(){
        if(this.is_dead == false){
            this.is_dead = true;
            console.log("player dead");
        }
        
    }

    
}



