<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.5.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <true/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>cocos2d-x</string>
        <key>textureFileName</key>
        <filename></filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">Basic</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>snow_man.plist</filename>
            </struct>
            <key>header</key>
            <key>source</key>
            <struct type="DataFile">
                <key>name</key>
                <filename></filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <false/>
        <key>trimSpriteNames</key>
        <false/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">None</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/bigfall00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/bigfall01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/bigfall02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/bigfall03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/bigfall04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/bigfall05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/bigfall06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/bigfall07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/bigfall08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/bigfall09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/bigfall10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/bubble.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/climb00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/climb01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/climb02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/climb03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/climb04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/climb05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/climb06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/climb07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/climb08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dangling00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dangling01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dangling02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dangling03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dangling04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dangling05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dangling06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dangling07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dangling08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dangling09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dash00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dash01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dash02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dash03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/deaddown00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/deaddown01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/deadside00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/deadside01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/deadup00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/deadup01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/death_h00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/death_h01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/death_h02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/death_h03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/death_h04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/death_h05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/death_h06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/death_h07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/death_h08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/death_h09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/death_h10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/death_h11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/death_h12.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/death_h13.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/death_h14.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash12.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash13.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash14.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash15.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash16.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash17.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash18.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash19.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/dreamDash20.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/duck.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge12.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge13.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge_back00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge_back01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge_back02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge_back03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge_back04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge_back05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge_back06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge_back07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge_back08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge_back09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge_back10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge_back11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge_back12.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/edge_back13.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fall00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fall01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fall02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fall03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fall04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fall05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fall06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fall07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fallPose00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fallPose01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fallPose02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fallPose03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fallPose04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fallPose05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fallPose06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fallPose07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fallPose08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fallPose09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/fallPose10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/flip00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/flip01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/flip02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/flip03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/flip04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/flip05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/flip06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/flip07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/flip08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/halfWakeUp00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/hug00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleA00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleA01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleA02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleA03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleA04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleA05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleA06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleA07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleA08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleA09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleA10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleA11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB12.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB13.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB14.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB15.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB16.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB17.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB18.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB19.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB20.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB21.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB22.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleB23.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleC00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleC01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleC02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleC03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleC04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleC05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleC06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleC07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleC08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleC09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleC10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idleC11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle_carry00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle_carry01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle_carry02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle_carry03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle_carry04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle_carry05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle_carry06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle_carry07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/idle_carry08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/jumpFast00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/jumpFast01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/jumpFast02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/jumpFast03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/jumpSlow00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/jumpSlow01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/jumpSlow02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/jumpSlow03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/jump_carry00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/jump_carry01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/jump_carry02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/jump_carry03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launch00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launch01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launch02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launch03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launch04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launch05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launch06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launch07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launchRecover00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launchRecover01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launchRecover02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launchRecover03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launchRecover04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launchRecover05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launchRecover06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launchRecover07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launchRecover08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launchRecover09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/launchRecover10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/lookUp00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/lookUp01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/lookUp02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/lookUp03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/lookUp04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/lookUp05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/lookUp06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/lookUp07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/pickup00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/pickup01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/pickup02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/pickup03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/pickup04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/push00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/push01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/push02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/push03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/push04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/push05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/push06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/push07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/push08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/push09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/push10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/push11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/push12.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/push13.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/push14.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/push15.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runFast00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runFast01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runFast02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runFast03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runFast04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runFast05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runFast06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runFast07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runFast08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runFast09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runFast10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runFast11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runSlow00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runSlow01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runSlow02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runSlow03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runSlow04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runSlow05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runSlow06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runSlow07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runSlow08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runSlow09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runSlow10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runSlow11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runStumble00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runStumble01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runStumble02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runStumble03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runStumble04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runStumble05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runStumble06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runStumble07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runStumble08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runStumble09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runStumble10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/runStumble11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_carry00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_carry01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_carry02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_carry03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_carry04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_carry05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_carry06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_carry07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_carry08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_carry09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_carry10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_carry11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_wind00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_wind01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_wind02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_wind03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_wind04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_wind05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_wind06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_wind07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_wind08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_wind09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_wind10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/run_wind11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/shaking00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sitDown00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sitDown01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sitDown02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sitDown03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sitDown04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sitDown05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sitDown06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sitDown07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sitDown08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sitDown09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sitDown10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sitDown11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sitDown12.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sitDown13.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sitDown14.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sitDown15.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep12.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep13.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep14.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep15.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep16.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep17.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep18.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep19.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep20.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep21.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep22.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/sleep23.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/spin00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/spin01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/spin02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/spin03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/spin04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/spin05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/spin06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/spin07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/spin08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/spin09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/spin10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/spin11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/spin12.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/spin13.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/starFly00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/startStarFly00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/startStarFly01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/startStarFly02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/startStarFly03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/startStarFlyWhite00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/startStarFlyWhite01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/startStarFlyWhite02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/startStarFlyWhite03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim12.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim13.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim14.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim15.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim16.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/swim17.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/throw00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/throw01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/throw02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/throw03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/tired00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/tired01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/tired02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/tired03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk11.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>8,8,16,16</rect>
                <key>scale9Paddings</key>
                <rect>8,8,16,16</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/hair00.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>3,3,5,5</rect>
                <key>scale9Paddings</key>
                <rect>3,3,5,5</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo00.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo01.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo02.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo03.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo04.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo05.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo06.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo07.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo08.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo09.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo10.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo11.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo12.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo13.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo14.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo15.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo16.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo17.png</key>
            <key type="filename">../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player/walk_carry_theo18.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>12,12,24,24</rect>
                <key>scale9Paddings</key>
                <rect>12,12,24,24</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>../../../../../../Downloads/mintincelestesmh_c3f68/Graphics/Atlases/Gameplay/SphxreMint/Mint/characters/player</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
